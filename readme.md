# Covert Communication and Data Exfilitration using TCP/IP
[brief description of program - mention defcon talk and other papers in documentation folder]



## Requirments

## How to Run

## Methods
### Storing Data
stores data within the seq field of the TCP header. Meaning, the data sent in one packet is limited
to 32-bits. Data greater than 32-bits in size is segmented and sent as individual packets, with the tcp
header field "window" being used to indicate, to the user, when they are recv the first segment and the last segment;
if the last segment requires padding this will be indicated in the TCP urgent pointer field, with the value being
set to that of the byte were the padding ends.

### Identifying Sent Packets
To allow the recviever to differentiate between the sender packets and everything other packet they recv, a specific
value will be set within the IP "id" header field.

### Data Encryption
While the entire point of this program is to send data covertly, the data is still encrypted to add another level of protection
againt potentially evesdroppers and prying eyes. The encryption is by no means strong or secure, but simply serves as a method
of providing some of obscuring the data so that it's original value is not readily avaible to potential adversaries.

The data is encrypted by using the constant as repeating key and XORing it with the data.
