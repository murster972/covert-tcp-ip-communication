#!/usr/bin/env python3
import math

class DataHandler:
    ''' handles binary data for covert communication,
        sender: encrypts and segments so will fit in 32-bits,
        recv: combines recieved segments and decrypts '''

    # size of segments in bytes - 32-bits (4 Bytes) is size of tcp seq field
    SEG_SIZE = 4

    def __init__(self, data, constant):
        ''' :param data: bytes, data user wishes to send
            :param constant: int, intial constant used for encrypting dmcflyata '''
        self.data = data
        self.constant = constant

    def encrypt(self):
        ''' encrypts data with constant as a repeating key, constant
            is first turned into binary and then acts as a repeating key with
            the datas bits being XORd with the constants subsequent bit

            :return list: encrypted segments '''
        repeating_key = self._constant_to_key(self.constant, len(self.data))

        # NOTE: uses big endian as it is the byte order used when sending data
        #       over a network
        encrypted = int.from_bytes(self.data, "big") ^ int.from_bytes(repeating_key, "big")
        encrypted = encrypted.to_bytes(len(self.data), "big")

        return self._segment(encrypted)

    def decrypt(self, segments, constant):
        combined_segments = self._desegment(segments)
        repeating_key = self._constant_to_key(constant, len(combined_segments))

        plaintext = int.from_bytes(combined_segments, "big") ^ int.from_bytes(repeating_key, "big")
        plaintext = plaintext.to_bytes(len(combined_segments), "big")

        return plaintext

    def _constant_to_key(self, c, l):
        ''' coverts constant to repeating key to meet length l or
            shortens constant if longer than l
            :param c: int, constant
            :param l: int, length of data (in bytes)
            :return bytes: repeating key of length l '''
        # convert constant to bytes and extend to match the length of the data
        constant_bytes = bytes([c])
        ld, lc = l, len(constant_bytes)

        if lc < ld:
            # extends key to be same length or longer, hence why round up when dividing
            # e.g. if constant.len is 8 and data.len is 10, quot will be 2 instead of 1
            quot = math.ceil(ld / lc)
            constant_bytes = constant_bytes * quot

        # shortens constant - if required - to match length of data
        constant_bytes = constant_bytes[:ld]

        return constant_bytes

    def _segment(self, data):
        ''' segments data (bytes) into segments of size (bytes) spefifed by the class variable SEG_SIZE
            :return list: list of segements, in bytes '''
        # segments = [(segment, padd_indicator)]
        # padd_indicator indicates the byte at which padding ends, -1 if no padding
        # e.g. if padd_indicator is 3 then the 4th byte is where data starts and padding ends
        segments = []

        l = len(data)

        if l >= DataHandler.SEG_SIZE:
            # splits data into 32-bit blocks
            for i in range(DataHandler.SEG_SIZE, l + 1, DataHandler.SEG_SIZE):
                s = data[i - DataHandler.SEG_SIZE:i]
                segments.append((s, -1))

        if l % DataHandler.SEG_SIZE != 0:
            # handles remainning data that is less than 32-bits
            remain_bytes = data[-(l % DataHandler.SEG_SIZE):]

            # padds remainning data
            # IDEA: could have padding as all 1s and then set some other field to indicate at which
            #       byte the padding begins
            padd_l = DataHandler.SEG_SIZE - len(remain_bytes)
            padd = bytes([255] * padd_l)

            segments.append((padd + remain_bytes, padd_l))

        return segments

    def _desegment(self, segments):
        ''' combines segments together to form original bytes
            :param segments: list, each item is (segment, offset) where offset indicates the byte at
                             which padding ends, is -1 if no padding
            :return bytes: combined bytes '''
        combined_bytes = bytes()

        for seg in segments:
            seg_int_val, offset = seg

            bytes_ = seg_int_val.to_bytes(DataHandler.SEG_SIZE, "big")

            # BUG-FIXED: seg size was taken into account padding, so last segment was incorrect
            if offset != -1: bytes_ = bytes_[offset:]

            combined_bytes += bytes_

        return combined_bytes

    def _update_constant(self, no_segms):
        ''' updates constant based on the number of segments sent '''
        self.constant += (no_segms % 2) + 1

        return self.constant

if __name__ == '__main__':
    f = open("test_data/Notes for moving.docx", "rb").read()
    e = DataHandler(f, 20).encrypt()

    e = [(int.from_bytes(i[0], "big"), i[1]) for i in e]

    d = DataHandler(f, 20).decrypt(e, 20)


    print(d == f)
    #d = DataHandler(f, 20).decrypt(e, 20)
