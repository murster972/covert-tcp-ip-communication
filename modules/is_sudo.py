#!/usr/bin/env python3
import os


def check_sudo():
	' Checks if user has sudo permissions '
	if os.getuid() != 0:
		print("[!] Program must be ran as root.")
		exit()