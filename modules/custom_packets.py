from scapy.all import *

# NOTE: config qireshark so that it doesnt show relative seq no, or won't show modified seq no

def custom_packet(ip_fields, tcp_fields):
    ''' Creates custom IP/TCP packets using scapy module
        :param ip_fields:  dict, values for ip fields e.g. src, dest, id, etc.
        :param tcp_fields: dict, values for tcp fields e.g. flags, seq, etc.
        :return bytes: bytes of constructred custom packet '''
    # init PDUs
    ip = IP()
    tcp = TCP()
    ether = Ether()

    valid_tcp_fields = ["sport", "dport", "seq", "ack", "dataofs", "reserved", "flags", "window", "chksum", "urgptr", "options"]
    valid_ip_fields = ["version", "ihl", "tos", "len", "id", "flags", "frag", "ttl", "proto", "chksum", "src", "dst", "options"]

    # check for invalid fields
    for i in [(valid_ip_fields, ip_fields), (valid_tcp_fields, tcp_fields)]:
        valids, fields = i
        invalid = [j for j in fields if j not in valids]

        if len(invalid) != 0:
            raise InvalidProtocolFields("Invalid Field/s: " + ", ".join(invalid))

    # set ip and tcp fields
    for field in ip_fields: setattr(ip, field, ip_fields[field])
    for field in tcp_fields: setattr(tcp, field, tcp_fields[field])

    # NOTE: order matters
    return ip/tcp

class InvalidProtocolFields(Exception):
    pass

if __name__ == '__main__':
    start_ind = int("10" * 8, 2)
    ip = {"src": "10.216.75.76", "dst": "8.8.8.8", "id": 202}
    tcp = {"dport": 20, "sport": 99, "seq": 1001, "window": start_ind, "urgptr": 1}
    packet = custom_packet(ip, tcp)

    send(packet, iface="wlp3s0")
