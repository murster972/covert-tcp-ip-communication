#!/usr/bin/env python3
from scapy.all import *
from threading import Thread

import time

from modules.data_handler import DataHandler
from modules.is_sudo import check_sudo

class Reciever:
    SEG_START_INDC = 43690
    SEG_END_INDC = 21845

    def __init__(self, _id, constant, save_location, iface):

        self.recv_segments = False
        self.segments = []

        self.constant = constant

        self.id = _id

        self.iface = iface
        self.save_location = save_location

        self.data_handler = DataHandler(constant, "")

    def listen(self):
        ''' listens for sender packets '''
        print("[*] Listening for packets from sender where: ID={}".format(self.id))

        sniffer = sniff(filter="ip", prn=self._filter, iface=self.iface)

    def _filter(self, packet):
        ''' checks if recv. packet is from the sender '''
        if packet[IP].id == self.id:
            self._handle_packet(packet)

    def _handle_packet(self, packet):
        ''' deals with packets sent by the sender
            :param segments: object, packet object from scapy '''
        seg_indc = packet[TCP].window
        data = packet[TCP].seq
        padd = packet[TCP].urgptr

        # sets padd to -1 to indicate no padding if urgptr value is 0
        padd = padd if padd != 0 else -1

        self.segments.append((data, padd))

        if seg_indc == Reciever.SEG_START_INDC:
            # packet is the first segment of msg
            print("[*] Recieving segments")
        elif seg_indc == Reciever.SEG_END_INDC:
            print("[+] Recieved all segments")

            # packet is the last segment of msg
            decrypted = self.data_handler.decrypt(self.segments, self.constant)

            self._save_data(decrypted)

            #self.constant = self.data_handler._update_constant(len(self.segments))
            self.segments = []
            self.recv_segments = False

    def _save_data(self, data):
        ''' saves decrypted data to file '''
        f_name = self.save_location + "/" + time.ctime()

        print("[*] Saving recieved data to file: {}".format(f_name))

        with open(f_name, "wb") as f: f.write(data)

if __name__ == '__main__':
    check_sudo()
    r = Reciever(202, 200, "save_location", "wlp3s0")
    r.listen()